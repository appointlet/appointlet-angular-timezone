describe("timezone provider", function() {
  'use strict';

  beforeEach(function() {
    module('appointlet.timezone');
  });

  it("should be defined", inject(function(Timezone) {
    expect(Timezone).toBeDefined();
  }));
});
