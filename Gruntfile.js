module.exports = function(grunt) {
  "use strict";

  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-karma');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    ngAnnotate: {
      timezone: {
        files: {
          'timezone.dist.js': ['timezone.js']
        }
      }
    },

    karma: {
      unit: {
        configFile: 'karma.conf.js',
      }
    },
  });

  grunt.registerTask('build', ['ngAnnotate']);
  grunt.registerTask('test', ['karma']);
  grunt.registerTask('default', ['build', 'test']);
};
