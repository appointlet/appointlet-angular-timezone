(function() {
  'use strict';

  angular.module('appointlet.timezone', ['angularMoment'])

  /**
   * @ngdoc service
   * @name timezone.service.Timezone
   *
   * @description
   * Wrappers for various translation/timezone libraries.
   */
  .service('Timezone', function($log, $rootScope, angularMomentConfig) {
    /**
     * @ngdoc function
     * @name timezone.service.Timezone#set
     *
     * @description
     * Sets the timezone.
     */
    this.set = function(zone) {
      $log.debug('Timezone', 'set', zone);
      angularMomentConfig.timezone = zone;
      $rootScope.$broadcast('timezone:changed', zone);
    };

    /**
     * @ngdoc function
     * @name timezone.service.Timezone#get
     *
     * @description
     * Returns the timezone.
     */
    this.get = function() {
      return angularMomentConfig.timezone;
    };

    /**
     * @ngdoc function
     * @name timezone.service.Timezone#localize
     *
     * @description
     * Converts a moment object to the current timezone.
     */
    this.localize = function(m) {
      return m.tz(this.get());
    };
  });

})();
